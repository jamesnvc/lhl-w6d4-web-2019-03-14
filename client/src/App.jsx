import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {messages: []};
  }

  componentDidMount() {
    this.socket = new WebSocket('ws://localhost:4000');
    this.socket.addEventListener('open', () => {
      console.log('Socket opened');
    });
    this.socket.addEventListener('message', (msgData) => {
      let msg = JSON.parse(msgData.data);
      console.log('Client recieved message:', msg);
      this.setState({messages: this.state.messages.concat([msg])});
    });
  }

  sendMessage(msg) {
    this.socket.send(JSON.stringify(msg));
  }

  render() {
    return (
      <div className="App">
        <Messages messages={this.state.messages}/>
        <MessageCreator send={this.sendMessage.bind(this)}/>
      </div>
    );
  }
}

const Messages = ({messages}) => {
  return (
    <>
      <h2>Messages</h2>
      <ul className="messages">
        { messages.map((m) => <Message key={m.id} message={m}/>) }
      </ul>
    </>
  );
};

const Message = ({message}) => {
  return (
    <li style={{color: message.colour}}> { message.content }
    </li>
  );
};

class MessageCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {input: ""};
  }

  inputChange(event) {
    this.setState({input: event.target.value });
  }

  keyDown(event) {
    if (event.key === 'Enter') {
      this.props.send({content: this.state.input});
      this.setState({input: ""});
    }
  }

  render() {
    return (
      <label>
        <span>Enter Message: </span>
        <input type="text"
               value={ this.state.input }
               onChange={ this.inputChange.bind(this) }
               onKeyDown={ this.keyDown.bind(this) }
        />
      </label>
    )
  }
}

export default App;
