// const express = require('express');
const SocketServer = require('ws').Server;
const uuid = require('uuid/v4');
const randomColour = require('random-color');

const PORT = 4000;
// const app = express().use(express.static('public'));
// app.listen(PORT, '0.0.0.0', 'localhost', () => {
//   console.log(`Started on port ${PORT}`);
// });

const wsServer = new SocketServer({port: PORT,
                                   host: 'localhost'});
SocketServer.prototype.broadcast = function(msg) {
  this.clients.forEach((client) => {
    client.send(JSON.stringify(msg));
  });
};

// if we wanted to group clients by user
let userClients = new Map();

wsServer.sendToUser = (userId, msg) => {
  userClients[userId].forEach((client) => {
    client.send(JSON.stringify(msg));
  });
};

// this would be where you'd get the cookie/session/HTTP Basic
// auth/whatever from the request and turn it into the userId for your app
const userFromReq = (req) => 101;

let userColour = {};

const commandHandlers = {
  '/msg': ({args}) => {
    // handling a message like
    // `/msg 123 hey, this is a private message`
    const [userToId, ...msgParts] = args;
    const msg = msgParts.join(' ');
    wsServer.sendToUser(userToId, {content: msg,
                                   id: uuid()});
  },
  '/smile': ({client}) => {
    wsServer.broadcast({content: "😁", id: uuid()});
  },
  '/colours': ({client}) => {
    client.send(JSON.stringify(
      {id: uuid(),
       content: Object.values(userColour).join("\n")
      }));
  },
  '/colour': ({args, clientId}) => {
    const matches = args[0].match(/^#([0-9a-f]{6})/i);
    if (matches) {
      console.log(matches);
      const oldColour = userColour[clientId] ;
      userColour[clientId] = matches[0];
      wsServer.broadcast({id: uuid(),
                          content: `Colour ${oldColour} => ${matches[0]}`
                         });
    }
  },
  '/doubled': ({args, client}) => {
    const msg = args[0];
    const matches = msg.match(/(\w+)\1/);
    if (matches) {
      client.send(JSON.stringify({id: uuid(),
                                  content: `${matches[1]} twice`}));
    } else {
      client.send(JSON.stringify({id: uuid(),
                                  content: "Nope"}));
    }
  }
};

wsServer.on('connection', (wsClient, req) => {
  console.log('Connected');
  // check the user id corresponding to this request
  // get the cookie/session whatever from the request
  const userId = userFromReq(req);
  let clients = (userClients[userId] || []);
  clients.push(userId);
  userClients.set(userId, clients);

  // for associating connections with a given colour
  const clientId = uuid();
  userColour[clientId] = randomColour().hexString();

  // notify all clients about new connection
  wsServer.broadcast({content: 'New user!',
                      id: uuid()});

  wsClient.on('message', (msgData) => {
    console.log(`Message from client: ${msgData}`);
    let msg = JSON.parse(msgData);
    if (msg.content.match(/^[/]/)) {
      const [command, ...args] = msg.content.split(/\s+/);
      commandHandlers[command]({args,
                                client: wsClient,
                                clientId});
    } else {
      msg.id = uuid();
      msg.colour = userColour[clientId];
      wsServer.broadcast(msg);
    }
  });

  wsClient.on('close', () => {
    console.log(`Client closed`);
    delete userColour[clientId];
  });
});
